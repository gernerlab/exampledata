# -*- coding: utf-8 -*-
"""
Created on Mon Apr 27 11:26:24 2020

@author: calebst

Thi script is designed to read and write to imaris hdf5 image files.
"""
import numpy as np
import h5py
import tkinter
from tkinter import ttk, StringVar
from tkinter.filedialog import askopenfilename
#%% Ask the user to define the file path

fpath = tkinter.filedialog.askopenfilename() 

#%% Manually define the file path

fpath  = 'C:\Users\calebst\Desktop\TEMP\Ilastic_Michael_Test\pSTAT5_merged_comped_g_Papain L Au 2 dLN.ims'
#%% Pull the channel names
FID = h5py.File(fpath, 'r')
data = FID.get('DataSetInfo')

print(data)

#%% Make some fake data and save it to a file

# create some random matrices
mat1 = np.random.random(size = (100,100))
mat2 = np.random.random(size = (5,7))
# print he matix sizes
print mat1.shape, mat2.shape

# create a test file
fpath = 'C:/Users/calebst/Desktop/TEMP/Ilastic_Michael_Test/TEST01.ims'
FID = h5py.File(fpath, 'w')
# add the matrices to the file
dat1 = FID.create_dataset('DataSet', data=mat1)
dat2 = FID.create_dataset('DataSetInfo', data=mat2)
# Now close the file and write the data to the disk
FID.close()
#%%
FID = h5py.File(fpath, 'r')
# Get the first layer of the file info
ls = list(FID.keys())
print('List of datasets in this file: \n ', ls)
# Start peeling layers to get to the filenames from DataSetInfo
ls2 = list(FID[ls[1]])
print('List of datasets in this file: \n ', ls2)


#Pull the indeces of the channel elements
Chind = [i for i, s in enumerate(ls2) if 'Channel' in s]
#Pull the number of channels
Nch = len(Chind)

# putt DataSetInfo
DatInfo = FID.get(ls[1])
# Pull a channel
DatInfo = DatInfo[ls2[Chind[1]]]
# Pull the attributes of that channel
chname = DatInfo.attrs['Name']


dataset1 = np.array(data)
print('Shape of DataSet:', dataset1.shape)


















